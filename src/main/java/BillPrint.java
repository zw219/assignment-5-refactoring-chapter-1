import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}


	public String statement() {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		int totalAmount = 0;
		int volumeCredits = 0;
		String result = "Statement for " + this.customer + "\n";
		for (Performance perf: performances) {
			Play play = plays.get(perf.getPlayID());
			if (play == null) {
				throw new IllegalArgumentException("No play found");
			}

			int thisAmount = 0;

			switch (play.getType()) {
				case "tragedy": thisAmount = 40000;
					            if (perf.getAudience() > 30) {
						             thisAmount += 1000 * (perf.getAudience() - 30);
					            }
								break;
				case "comedy":  thisAmount = 30000;
					            if (perf.getAudience() > 20) {
						            thisAmount += 10000 + 500 * (perf.getAudience() - 20);
					            }
					            thisAmount += 300 * perf.getAudience();
								break;
				default:        throw new IllegalArgumentException("unknown type: " +  play.getType());
			}

			// add volume credits
			volumeCredits += Math.max(perf.getAudience() - 30, 0);
			if ( play.getType().equals("comedy")) {
				volumeCredits += Math.floor((double) perf.getAudience() / 5.0);
			}

			// print line for this order
			result += "  " + play.getName() + ": $" + numberFormat.format((double) thisAmount / 100.00) + " (" + perf.getAudience()
					+ " seats)" + "\n";
			totalAmount += thisAmount;
		}
		result += "Amount owed is $" + numberFormat.format((double) totalAmount / 100) + "\n";
		result += "You earned " + volumeCredits + " credits" + "\n";
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
